package org.example.store.support.ui;

import java.util.List;

import org.example.store.page.HomePage;
import org.openqa.selenium.WebElement;

public class MainMenu {
	private HomePage page;
	private List<WebElement> items;

	public MainMenu(HomePage page, List<WebElement> items) {
		this.page = page;
		this.items = items;
	}
	
	public HomePage click(String content) {
		for (WebElement item : items) {
			if (item.getAttribute("content").equals(content)) {
				item.click();
				break;
			}
		}
		return page;
	}
}

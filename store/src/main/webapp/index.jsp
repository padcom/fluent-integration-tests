<!doctype html>

<html>

<head>
	<title>Example application</title>

	<style>
		html, body { font-family: arial; border: none; margin 0; padding: 0; }
		#page { width: 700px; height: 400px; border: solid 1px black; padding: 5px; background-color: #eee; }
		#page-title { font-size: 30px; font-family: arial; font-weight: bold; padding: 5px; }
		#userinfo { float: right; font-size: 15px; font-weight: normal; margin-top: 0px; }
		.menu { background-color: gray; padding: 10px; }
		.menu li { padding: 10px 20px; margin: 0; display: inline; list-style: none; }
		.menu li a { text-decoration: none; color: black; }
		.menu .selected { background-color: green; }
		.menu li:hover { background-color: red; color: white; }
		#content { width: 600px; }
	</style>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".menu").on("click", "a", function() {
				$(".menu li").removeClass("selected");
				$(this).parent().addClass("selected");
				$("#content").html("You're browsing " + $(this).attr("content"));
			});
		});
	</script>
</head>

<body>
	<div id="page">
		<div id="header">
			<span id="page-title">Super store!</span>
			<div id="userinfo">
				Logged in as <span id="userid">${pageContext.request.remoteUser}</span>
				|
				<a href="${pageContext.request.contextPath}/logout">Logout</a>
			</div>
		</div>
		<ul class="menu">
			<li><a href="#" content="fruits">Fruits</a></li>
			<li><a href="#" content="meat">Meat</a></li>
			<li><a href="#" content="alcohol">Alcohol</a></li>
		</ul>
		<div id="content"></div>
	</div>
</body>

</html>

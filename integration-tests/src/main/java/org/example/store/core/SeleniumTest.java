package org.example.store.core;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class SeleniumTest {
	protected static WebDriver driver;

	@Before
	public void startupWebDriver() {
		if (driver == null) {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setJavascriptEnabled(true);
			driver = new FirefoxDriver(capabilities);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						driver.quit();
					} catch (Throwable e) {
					}
				}
			}));
		}
	}
}

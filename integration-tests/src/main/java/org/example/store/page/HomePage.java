package org.example.store.page;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.example.store.core.Page;
import org.example.store.support.ui.MainMenu;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {
	@FindBy(id = "userid")
	WebElement username;

	@FindBy(linkText = "Logout")
	WebElement logoutLink;

	@FindBy(css = ".menu li a")
	List<WebElement> menu;

	@FindBy(id = "content")
	WebElement content;
	
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public HomePage assertIsUserLoggedIn(String username) {
		assertThat(this.username.getText()).isEqualTo(username);
		return this;
	}
	
	public void logout() {
		logoutLink.click();
	}

	public HomePage gotoContent(String content) {
		return new MainMenu(this, menu).click(content);
	}
	
	public HomePage assertContentContains(String data) {
		assertThat(content.getText()).contains(data);
		return this;
	}
}

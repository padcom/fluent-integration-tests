package org.example.store;

import org.example.store.core.SeleniumTest;
import org.example.store.page.LoginPage;
import org.example.store.scenario.LoginJohnDoeScenario;
import org.example.store.scenario.SelectContentScenario;
import org.junit.Test;

public class ContentSelectTest extends SeleniumTest {
	@Test
	public void will_select_fruits() {
		new LoginPage(driver)
			.run(new LoginJohnDoeScenario())
			.run(new SelectContentScenario().content("fruits"))
			.run(new SelectContentScenario().content("alcohol"))
			.logout();
	}
}

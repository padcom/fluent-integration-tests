package org.example.store.scenario;

import org.example.store.core.Scenario;
import org.example.store.page.HomePage;
import org.example.store.page.LoginPage;

public class LoginJohnDoeScenario implements Scenario<LoginPage, HomePage> {
	@Override
	public HomePage run(LoginPage entry) {
		return entry
				.login("johndoe", "s3cret")
				.assertIsUserLoggedIn("johndoe");
	}
}

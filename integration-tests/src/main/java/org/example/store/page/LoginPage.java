package org.example.store.page;

import org.example.store.core.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {
	private static final String URL = "http://localhost:8080/store";

	@FindBy(name = "j_username")
	WebElement username;

	@FindBy(name = "j_password")
	WebElement password;

	@FindBy(css = "input[type='submit']")
	WebElement loginButton;

	public LoginPage(WebDriver driver) {
		super(driver, URL);
	}
	
	public HomePage login(String username, String password) {
		this.username.clear();
		this.username.sendKeys(username);
		this.password.clear();
		this.password.sendKeys(password);
		this.loginButton.click();
		return new HomePage(driver);
	}
}

package org.example.store.scenario;

import org.example.store.core.Scenario;
import org.example.store.page.HomePage;

public class SelectContentScenario implements Scenario<HomePage, HomePage> {
	private String content = "fruit";
	
	public SelectContentScenario content(String content) {
		this.content = content;
		return this;
	}
	
	@Override
	public HomePage run(HomePage entry) {
		return entry
				.gotoContent(content)
				.assertContentContains(content);
	}
}

package org.example.store.core;


public interface Scenario<Input extends Page, Output extends Page> {
	Output run(Input entry);
}

package org.example.store.core;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Page {
	protected final WebDriver driver;
	
	public Page(WebDriver driver) {
		this(driver, null);
	}
	
	public Page(WebDriver driver, String url) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		if (!isEmpty(url)) {
			driver.navigate().to(url);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <Input extends Page, Output extends Page> Output run(Scenario<Input, Output> scenario) {
		return scenario.run((Input) this);
	}
}

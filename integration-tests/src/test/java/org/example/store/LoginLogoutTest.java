package org.example.store;

import org.example.store.core.SeleniumTest;
import org.example.store.page.LoginPage;
import org.example.store.scenario.LoginJohnDoeScenario;
import org.junit.Test;

public class LoginLogoutTest extends SeleniumTest {
	@Test
	public void will_login_and_logout() {
		new LoginPage(driver)
			.run(new LoginJohnDoeScenario())
			.logout();
	}
}
